// components/component-a/component-a.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    name: String,
    year:String,
    age: {
      type: Number,
      value: 100 //默认值
    },
    address: {
      type: Object,
      value: {
        provice: "广东",
        city: "深圳"
      }
    }
  },

  // 组件的生命周期
  // lifetimes是组件的的生命周期声明字段，优先级最高，推荐
  lifetimes: {
    created() {
      // 在组件实例刚刚被创建时执行
      console.log('---created---');
    },
    attached() {
      // 在组件实例进入页面节点树时执行
      console.log('---attached---');
    },
    ready() {
      // 在组件在视图层布局完成后执行
      console.log('---ready---',this);
    },
    detached() {
      // 在组件实例被从页面节点树移除时执行
      console.log('---detached---');
    },
  },

/**
 * 组件的初始数据
 */
data: {

},

/**
 * 组件的方法列表
 */
methods: {
  sentValueToParent(){
    this.triggerEvent("myevent",[{
      name:'张三',
      nickname:'法外狂徒'
    },{
      name:'李四',
      nickname:'张三好友'
    }
  ])
  }
}
})