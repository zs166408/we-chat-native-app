// app.js
App({
  globalData:{
    person:{
      name:'蔡徐坤',
      age:30,
      address:'湖南'
    }
  },

  // 小程序启动的时候(这个函数只会执行一次)
  onLaunch(options) {
    // console.log('---App---onLaunch---',options);
    // 把场景值发给后台
  },

  // 应用程序可以看到了(可能执行很多次)
  onShow(options){
    // console.log('---App---onShow---',options);
    // 把场景值发给后台
  },

  // 应用程序隐藏了
  onHide(){
    // console.log('---App---onHide---');
  }
})
