// packageB/pages/goods-detail/goods-detail.js

Page({
  // 页面初始数据
  data:{},

  // 生命周期--监听页面加载
  onLoad(options) {
    console.log('---options---',options);
  },

  // 跳转购物车
  goToCart() {
    wx.switchTab({
      url: '/pages/cart/cart',
    })
  },
  onUnload() {
    console.log('---onUnload---')
  }
})