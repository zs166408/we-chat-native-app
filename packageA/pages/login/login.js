import {
  login
} from '../../../api/user'
Page({
  // 页面初始数据
  data: {
    loginname: '17704051019',
    password: '123456'
  },

  changeValue(e) {
    this.setData({
      // 属性名表达式：https://es6.ruanyifeng.com/#docs/object#%E5%B1%9E%E6%80%A7%E5%90%8D%E8%A1%A8%E8%BE%BE%E5%BC%8F
      [e.target.dataset.name]: e.detail
    })
  },

  async onLogin() {
    try {
      const res = await login({
        loginname: this.data.loginname,
        password: this.data.password
      })

      // 校验手机号
      const reg = /^1[3-9]\d{9}$/
      if(!reg.test(this.data.loginname)){
        wx.showToast({
          title: '手机号错误',
          icon:'none'
        })
        return
      } 

      // 校验密码
      // if(this.data.password.trim() == ''){
      //   wx.showToast({
      //     title: '密码不能为空',
      //     icon:'none'
      //   })
      //   return
      // }
      if (this.data.password == '') {
        console.log('this.data.password');
        wx.showToast({
          title: '密码不能为空!',
          icon: 'none'
        })
        return
      }

      // 判断请求获取的状态码

      if (res.code === 200) {
       setTimeout(() => {
          // wx.reLaunch(Object object)
        // 关闭所有页面，打开到应用内的某个页面
        wx.reLaunch({
          url: '/pages/home/home',
        })
       },800)

        // 提示
        wx.showToast({
          title: res.message,
          icon: 'success'
        })

        // 保存token到本地
        // wx.setStorageSync(string key, any data)
        // 将数据存储在本地缓存中指定的 key 中。会覆盖掉原来该 key 对应的内容。除非用户主动删除或因存储空间原因被系统清理，否则数据都一直可用。
        wx.setStorageSync('token', res.data.token)
        // wx.setTabBarItem(Object object)
        // 动态设置 tabBar 某一项的内容
        wx.setTabBarItem({
          index: 3,
          text: "我的"
        })
      }
    } catch (err) {
    }
  }


})