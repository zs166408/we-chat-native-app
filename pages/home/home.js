// pages/home/home.js
import {request} from '../../utils/request'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    swipers:[],//轮播图
    menus: [], // 菜单分类列表
    goodsList:[],//商品列表
    page:1,//页码
    pageSize:10,//页容量
    hasMore:true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getSwiperData(),
    this.getMenuListData(),
    this.getGoodsListData()
  },

  // 获取轮播图数据
  // getSwiperData() {
  //   wx.request({
  //     url: 'http://huangjiangjun.top:3001/api/banner/list',
  //     success:(res) => {
  //       this.setData({
  //         swipers:res.data.data
  //       },() => {
  //         // 这个函数相当于是setData执行完成之后的回调函数
  //         // 我们如果想要在 setData 做完事情之后，再去获取数据，最好写在第二个参数中
  //         console.log('---res---' , this.data.swipers);
  //       })
  //     }
  //   })
  // },

  // 获取轮播图数据
  async getSwiperData() {
    const res = await request({url:'banner/list'})
    // console.log(res);
    this.setData({
      swipers:res.data
    })
  },

  // 获取菜单列表数据
//   getMenuListData() {
//     wx.request({
//       url: 'http://huangjiangjun.top:3001/api/menu/list',
//       success:(res) => {
//         this.setData({
//           menus:res.data.data
//         },() => {
//           // 这个函数相当于是setData执行完成之后的回调函数
//           // 我们如果想要在 setData 做完事情之后，再去获取数据，最好写在第二个参数中
//           console.log('---res---' , this.data.menus);
//     })
//   }


// })

// }

// 获取菜单列表数据
async getMenuListData(){
  const res = await request({url:'menu/list'})
  this.setData({
    menus:res.data
  })
},

// 获取商品列表数据
async getGoodsListData(){
  // this.data.page++
  const res = await request({
    url:'pro/list',
    data:{
      count:this.data.page,
      limitNum:this.data.pageSize
    }
  })

  // this.setData({
  //   hasMore: res.data.length > 0
  // })

   // 停止掉下刷新的效果
   wx.stopPullDownRefresh()

   this.setData({
     hasMore: res.data.length > 0
   })

  //  修改模型和视图数据
  this.setData({
    // goodsList: res.data.data
    goodsList: [...this.data.goodsList,...res.data],
    hasMore: res.data.length > 0
  })
},

 // 上拉加载更多
onReachBottom() {
  if (!this.data.hasMore) return

  this.data.page++
  console.log('---onReachBottom---');
  this.getGoodsListData()
},

// 下拉刷新
onPullDownRefresh(){
  console.log('---onPullDownRefresh---');

  // 页码重置为1
  this.data.page = 1
  // 把内容清空
  this.setData({
    goodsList: []
  })
  
  this.getGoodsListData()
},

// 编程式导航跳转商品详情页面
goToDetail(e){
  wx.navigateTo({
    url: '/packageB/pages/goods-detail/goods-detail?proid=' + e.currentTarget.dataset.proid
  })
},

onShow() {
  // 还是根据token来判断是否登录了
  const token = wx.getStorageSync('token')
  wx.setTabBarItem({
    index: 3,
    text: token ? '我的' : '未登录'
  })
}


})