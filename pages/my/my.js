// pages/my/my.js
import Dialog from '@vant/weapp/dialog/dialog'
import { getUserInfo } from '../../api/my'

Page({
  // 页面的初始数据
  data: {
    userInfo: {}
  },

  //  生命周期函数--监听页面显示
  onShow() {
    // any wx.getStorageSync(string key)
    // 从本地缓存中同步获取指定 key 的内容
    const token = wx.getStorageSync('token')
    if (!token) {
      // 关闭当前页面，跳转到应用内的某个页面。但是不允许跳转到 tabbar 页面
      wx.redirectTo({
        url: '/packageA/pages/login/login',
      })
    }else{
      // 获取用户数据进行渲染
      this.getUserInfoData()
    }
  },

  async getUserInfoData() {
    const res = await getUserInfo()
    this.setData({
      userInfo:res.data
    })
  },

  // 退出
  logout(){
    Dialog.confirm({
      title:'提示',
      message:'确认退出吗？'
    }).then(() => {
      // on confirm
      wx.removeStorageSync('token')
      wx.redirectTo({
        url: '/packageA/pages/login/login',
      })
    })
  }

})