// pages/category/category.js

import {
  request
} from '../../utils/request'

Page({
  // 页面初始数据
  data: {
    category: [],
    currentIndex: 0
  },

  // 生命周期--监听页面加载
  onLoad(options) {
    this.getCategories (),
    console.log(this.getCategories ())
  },

  // 发起请求获取分类数据
  async getCategories () {
    const res = await request({url:'category/list'})
    // console.log(res);
    this.setData({
      category:res.data
    })
  },

  // 切换一级分类
  switchCate(e) {
    console.log(e)
    this.setData({
      currentIndex:e.target.dataset.id -1
      // currentIndex: e.target.dataset.index
    })
  }

})