// pages/third/third.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isShow: true,
    address: {
      provice: '广东',
      city: '佛山'
    },
  },

  // 生命周期-监听页面加载
  onLoad() {
    // console.log('---onLoad---',app.globalData);
    // console.log('third---onLoad---');
  },
  // 生命周期回调—监听页面显示
  onShow() {
    // console.log('third---onShow---');
  },
  // 生命周期回调—监听页面初次渲染完成
  onReady() {
    // console.log('third---onReady---');
  },
  // 生命周期回调—监听页面隐藏
  onHide() {
    // console.log('third---onHide---');
  },
  // 生命周期回调—监听页面卸载
  onUnload() {
    // console.log('third---onUnload');
  },

  getValue(e){
    console.log('---getValue---',e);
  },

  getUserInfo(e){
    console.log('---getUserInfo---',e);
  },

  wxLogin(){
    wx.getUserProfile({
      desc: '是否允许嗨购商城为您提供服务',
      success: res => {
        console.log(res);
      },

      fail: err => {
        console.log(err)
      }
    })
  },

  login(){
    wx.login({
      success: (res) => {
        console.log('---res---',res)
        wx.request({
          url: 'https://api.weixin.qq.com/sns/jscode2session',
          data:{
            "appid":"wxef8578e9f63faccc",
            "secret":"6802f8282aaa7951c8e7bc2066c67706",
            "js_code":res.code,
            "grant_type":"authorization_code"
          },
          success:(res) => {
            console.log('---res---',res);
          }
        })
      },
    })
  }

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  // onPullDownRefresh() {

  // },

  

})