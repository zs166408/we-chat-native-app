// index.js
const app = getApp()


Page({
 
  data:{
    name:'法外狂徒张三',
    isMan:true,
    age:18,
    address:{
      provice:'广东',
      city:'深圳'
    },
    badMans:[
      {
        id:1001,name:'张三',address:'北京'
      },
      {
        id:1002,name:'李四',address:'上海'
      },
      {
        id:1003,name:'王五',address:'广州'
      },
      {
        id:1004,name:'赵六',address:'深圳'
      }
    ]
  },
    
    // 生命周期-监听页面加载
    onLoad(){
      // console.log('---onLoad---',app.globalData);
      console.log('index---onLoad---');
    },
    // 生命周期回调—监听页面显示
    onShow(){
      console.log('index---onShow---');
    },
    // 生命周期回调—监听页面初次渲染完成
    onReady(){
      console.log('index---onReady---');
    },
    // 生命周期回调—监听页面隐藏
    onHide(){
      console.log('index---onHide---');
    },
    // 生命周期回调—监听页面卸载
    onUnload(){
      console.log('index---onUnload');
    },

    changeValue() {
      // this.data.name = '正当防卫陈鹤皋'
      // console.log('---changeValue---',this.data);
      this.setData({
        name:'正当防卫陈鹤皋'
      })
    },

    goToSecond() {
      // vue2:this.$router.push('/second)
      // vue3:const router = useRouter()  router.push('/second)
      // 小程序相关的动作或操作看API
      // 小程序中的相关API调用的时候，都是以 wx: 开头
      wx.navigateTo({
        // 注意：根路径不要少，最后面不要加后缀
        url:'/pages/second/second'
      })
    }
    
 
})
