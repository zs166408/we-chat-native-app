// pages/second/second.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  tapParent() {
    console.log('---tapParent---tap---')
  },
  tapClild(e) {
    console.log('---tapChild---',e.target.dataset)
    // console.log('---tapChild---',e.currentTarget.dataset)
  },
 // 生命周期-监听页面加载
 onLoad(){
  // console.log('---onLoad---',app.globalData);
  console.log('second---onLoad---');
},
// 生命周期回调—监听页面显示
onShow(){
  console.log('second---onShow---');
},
// 生命周期回调—监听页面初次渲染完成
onReady(){
  console.log('second---onReady---');
},
// 生命周期回调—监听页面隐藏
onHide(){
  console.log('second---onHide---');
},
// 生命周期回调—监听页面卸载
onUnload(){
  console.log('second---onUnload');
},
goToThird() {
  wx.navigateTo({
    // 注意点：前面/要写上，最后不要写后缀
    url: '/pages/third/third',
  })
},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})