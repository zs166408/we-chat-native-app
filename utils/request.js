const BASEURL = 'http://huangjiangjun.top:3001/api/'

export const request = ({
  url,
  method = "GET",
  data = {},
  header = {}
}) => {

  // any wx.getStorageSync(string key)
  // 从本地缓存中同步获取指定 key 的内容
  const token = wx.getStorageSync('token')
  if (token) {
    header.token = token
  }

  return new Promise((resolve, reject) => {
    wx.request({
      // 使用wx.request发请求
      url: `${BASEURL}${url}`,
      method,
      data,
      header,
      success: res => {
        // 封装完毕之后，相当于一个请求拦截器
        console.log('---res---', res);
        // res整个响应数据，包括响应头，响应体，状态行

        if (res.statusCode === 401) {
          // 清空token
          wx.removeStorageSync('token')
          // 跳转
          wx.reLaunch({
            url: '/packageA/pages/login/login',
          })
          return
        }
        resolve(res.data)
      },
      fail: err => {
        reject(err)
      }

    })

  })
}